﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConexaoSQLServerSQLClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection conexao = new SqlConnection(@"Data Source=M002158\MSSQLSERVER01;Initial Catalog=banco_aula;Uid=rafa;pwd=123456;");
            SqlCommand sql = new SqlCommand("select * from bairro", conexao);
            try
            {
                conexao.Open();
                SqlDataReader lendoDados = sql.ExecuteReader();
                while (lendoDados.Read())
                {
                    richTextBox1.Text += lendoDados["bai_codigo"] + " - " + lendoDados["bai_nome"] + "\r\n";
                }
                lendoDados.Close();
                conexao.Close();

            }
            catch (SqlException erro)
            {
                MessageBox.Show("Erro = " + erro);
            }
        }
    }
}
